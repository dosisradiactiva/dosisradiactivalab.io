---
layout: post  
title: "Bienvenidos a la Familia Radiactiva"  
date: 2023-06-26  
categories: blog  
image: images/356989253_255184880554075_129256371588625905_n.jpg   
podcast_link: 
tags: [podcast, radiología]  
comments: true 
---

Agradezco mucho que me apoyen en este proyecto, espero que les sea fructifero el tiempo que pasen en el blog y el podcast. Tomaré en cuenta cada comentario de manera objetiva para crear contenido de mayor valor y calidad.

